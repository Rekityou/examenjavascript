const btnGenerar = document.getElementById('btnGenerar');
btnGenerar.addEventListener('click', function() {
    let edades = [];
    let bebe = 0, nino = 0, adolescentes = 0, adultos = 0, ancianos = 0;

    for (let i = 0; i < 100; i++) {
        let edad = Math.floor(Math.random() * 91);
        edades.push(edad);

        if (edad >= 1 && edad <= 3) bebe++;
        else if (edad >= 4 && edad <= 12) nino++;
        else if (edad >= 13 && edad <= 17) adolescentes++;
        else if (edad >= 18 && edad <= 60) adultos++;
        else if (edad >= 61) ancianos++;
    }

    document.getElementById('edades').innerText = edades.join(', ');
    document.getElementById('bebe').innerText = bebe;
    document.getElementById('nino').innerText = nino;
    document.getElementById('adolescentes').innerText = adolescentes;
    document.getElementById('adultos').innerText = adultos;
    document.getElementById('ancianos').innerText = ancianos;
});

const btnLimpiar = document.getElementById('btnLimpiar');
btnLimpiar.addEventListener('click', function() {
    document.getElementById('edades').innerText = '';
    document.getElementById('bebe').innerText = '0';
    document.getElementById('nino').innerText = '0';
    document.getElementById('adolescentes').innerText = '0';
    document.getElementById('adultos').innerText = '0';
    document.getElementById('ancianos').innerText = '0';
});
