const btnCalcular = document.getElementById('btnCalcular');
btnCalcular.addEventListener('click', function(){

    let cant = document.getElementById('txtCantidad').value;
    let res = 0;

    let CelFar = document.getElementById('cToF').checked;
    let FarCel = document.getElementById('fToC').checked;

    if(CelFar) {
        res = ((9/5) * cant ) + 32;
    } else if(FarCel) {
        res = (cant - 32 ) * (5/9);
    }

    document.getElementById('txtResultado').value = res.toFixed(2);

});

const btnLimpiar = document.getElementById('btnLimpiar');
btnLimpiar.addEventListener('click', function(){

    txtResultado.value = "";
    txtCantidad.value = "";

});
